package se.alexander.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "courses")
@NamedQueries({
        @NamedQuery(name = "Courses.findAll", query = "SELECT c FROM Course c"),
        @NamedQuery(name = "Courses.findCourseById", query = "SELECT c FROM Course c WHERE c.id = :id")})
public class Course implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @Column(name = "course_name")
    private String courseName;
    @NotNull
    @Column(name = "course_code")
    private String courseCode;
    @NotNull
    private String difficulty;
    @NotNull
    @Column(name = "amount_of_students")
    private String amountOfStudents;
    @NotNull
    private String teacher;
    @NotNull
    private String duration;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "student_course",
            joinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"))
    private List<Student> studentList;

    public Course() {
    }

    public Course(String courseName, String courseCode, String difficulty,
                  String amountOfStudents, String teacher, String duration) {
        this.courseName = courseName;
        this.courseCode = courseCode;
        this.difficulty = difficulty;
        this.amountOfStudents = amountOfStudents;
        this.teacher = teacher;
        this.duration = duration;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getAmountOfStudents() {
        return amountOfStudents;
    }

    public void setAmountOfStudents(String amountOfStudents) {
        this.amountOfStudents = amountOfStudents;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teachers) {
        this.teacher = teachers;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> students) {
        this.studentList = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        if (id != course.id) return false;
        if (!courseName.equals(course.courseName)) return false;
        if (!courseCode.equals(course.courseCode)) return false;
        if (!difficulty.equals(course.difficulty)) return false;
        if (!amountOfStudents.equals(course.amountOfStudents)) return false;
        if (!teacher.equals(course.teacher)) return false;
        return duration.equals(course.duration);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + courseName.hashCode();
        result = 31 * result + courseCode.hashCode();
        result = 31 * result + difficulty.hashCode();
        result = 31 * result + amountOfStudents.hashCode();
        result = 31 * result + teacher.hashCode();
        result = 31 * result + duration.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s, [%s, %s, %s, %s, %s, %s]",
                Student.class.getSimpleName(), courseName, courseCode, difficulty, amountOfStudents, teacher, duration);
    }
}
