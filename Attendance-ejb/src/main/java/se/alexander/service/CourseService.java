package se.alexander.service;

import se.alexander.domain.Course;
import se.alexander.domain.Student;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015-09-14.
 */
@Stateless
public class CourseService implements CourseServiceLocal {

    @PersistenceContext
    EntityManager entityManager;

    public CourseService() {
    }

    @Override
    public void addCourse(Course course) {
        entityManager.persist(course);
    }

    @Override
    public List<Course> getCourses() {
        TypedQuery<Course> query = entityManager.createNamedQuery("Courses.findAll", Course.class);
        List<Course> courses = query.getResultList();
        return courses;
    }

    @Override
    public void addStudentToCourse(Student student, int id) {
        Course course = findCourseById(id);
        List<Student> students = course.getStudentList();
        students.add(student);

        course.setStudentList(students);
        entityManager.merge(course);
    }

    @Override
    public Course findCourseById(int id) {
        try {
            Query query = entityManager.createNamedQuery("Courses.findCourseById").setParameter("id", id);
            Course course = (Course) query.getSingleResult();
            return course;
        } catch (NoResultException ex) {
            ex.printStackTrace();
            ex.getMessage();
            return null;
        }
    }

    @Override
    public Course editCourse(Course course) {
        Course c = findCourseById(course.getId());
        c.setCourseName(course.getCourseName());
        c.setCourseCode(course.getCourseCode());
        c.setAmountOfStudents(course.getAmountOfStudents());
        c.setDifficulty(course.getDifficulty());
        c.setTeacher(course.getTeacher());
        c.setDuration(course.getDuration());
        return entityManager.merge(c);
    }

    @Override
    public void removeCourseById(int id) {
        Course course = findCourseById(id);
        entityManager.remove(course);
    }

    @Override
    public void removeStudentsFromCourse(int courseId, Student student) {
        Course course = findCourseById(courseId);
        course.getStudentList().remove(student);
        entityManager.merge(course);
    }
}
