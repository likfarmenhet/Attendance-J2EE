package se.alexander.service;

import se.alexander.domain.Student;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015-09-10.
 */
@Stateless
public class StudentService implements StudentServiceLocal {

    @PersistenceContext
    EntityManager entityManager;

    public StudentService() {
    }

    @Override
    public void addStudent(Student student) {
        entityManager.persist(student);
    }

    @Override
    public List<Student> getStudents() {
        TypedQuery<Student> query = entityManager.createNamedQuery("Students.findAll", Student.class);
        List<Student> students = query.getResultList();
        return students;
    }

    @Override
    public Student findStudentById(int id) {
        try {
            Query query = entityManager.createNamedQuery("Students.findStudentById").setParameter("id", id);
            Student student = (Student) query.getSingleResult();
            return student;
        } catch (NoResultException ex) {
            ex.printStackTrace();
            ex.getMessage();
            return null;
        }
    }

    @Override
    public Student editStudent(Student student) {
        Student s = findStudentById(student.getId());
        s.setFirstName(student.getFirstName());
        s.setLastName(student.getLastName());
        s.setPhoneNumber(student.getPhoneNumber());
        s.setEmail(student.getEmail());
        return entityManager.merge(s);
    }

    @Override
    public void removeStudentById(int id) {
        Student student = findStudentById(id);
        entityManager.remove(student);
    }

}
