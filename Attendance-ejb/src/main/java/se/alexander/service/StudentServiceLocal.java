package se.alexander.service;

import se.alexander.domain.Student;

import java.util.List;

/**
 * Created by Alexander on 2015-09-10.
 */
public interface StudentServiceLocal {

    void addStudent(Student student);

    List<Student> getStudents();

    Student findStudentById(int id);

    Student editStudent(Student student);

    void removeStudentById(int id);

}
