package se.alexander.service;

import se.alexander.domain.Course;
import se.alexander.domain.Student;

import java.util.List;

/**
 * Created by Alexander on 2015-09-14.
 */
public interface CourseServiceLocal {

    void addCourse(Course course);

    List<Course> getCourses();

    void addStudentToCourse(Student student, int id);

    Course findCourseById(int id);

    Course editCourse(Course course);

    void removeCourseById(int id);

    void removeStudentsFromCourse(int courseId, Student student);

}
