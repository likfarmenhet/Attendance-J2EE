package se.alexander.beans;

import se.alexander.domain.Student;
import se.alexander.service.StudentServiceLocal;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015-09-10.
 */
@ManagedBean
@ViewScoped
public class StudentBean {

    private Student student;

    @Inject
    StudentServiceLocal studentServiceLocal;

    @PostConstruct
    public void init() {
        student = new Student();
    }

    public void addStudent() {
        studentServiceLocal.addStudent(student);
        student = new Student();
    }

    public void editStudent() {
        studentServiceLocal.editStudent(student);
    }

    public void findStudent() {
        this.student = studentServiceLocal.findStudentById(this.student.getId());
    }

    public void removeStudent(int id) {
        studentServiceLocal.removeStudentById(id);
    }

    public List<Student> getStudents() {
        return studentServiceLocal.getStudents();
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

}
