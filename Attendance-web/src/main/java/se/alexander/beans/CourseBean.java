package se.alexander.beans;

import se.alexander.domain.Course;
import se.alexander.domain.Student;
import se.alexander.service.CourseServiceLocal;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 2015-09-14.
 */
@ManagedBean
@ViewScoped
public class CourseBean {

    private Course course;

    @Inject
    CourseServiceLocal courseServiceLocal;

    @PostConstruct
    public void init() {
        course = new Course();
    }

    public void addCourse() {
        courseServiceLocal.addCourse(course);
        course = new Course();
    }

    public void editCourse() {
        courseServiceLocal.editCourse(course);
    }

    public void findCourse() {
        this.course = courseServiceLocal.findCourseById(this.course.getId());
    }

    public void removeCourse(int id) {
        courseServiceLocal.removeCourseById(id);
    }

    public List<Course> getCourses() {
        return courseServiceLocal.getCourses();
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public void addStudentToCourse(Student student, int id) {
        courseServiceLocal.addStudentToCourse(student, id);
    }

    public void removeStudentsFromCourse(int courseId, Student student) {
        courseServiceLocal.removeStudentsFromCourse(courseId, student);
    }

}
